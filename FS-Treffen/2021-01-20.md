# Protokoll FS-Treffen MaPhIn am 20.01.2021

## Anwesende
Marian Schnell, Miquel Vock, Adrian Breding, Ben Eltschig, Florian Kaufmann, Hannes Reichle, Jasha Maximilan Riebler, Johannes, Leonie Thaben, Linus Dürr, Lois, Steffen Blank, Tim, Tom Reinhardt, Yuuma Odaka-Falush, Max Wehmeier, Thilo, Jonas Bräuer, Anton Lembcke, Basti

(Laura Michaelis) Studentische Studienberatung IMP

\*: kommt später
(): ist zu Gast

## TOPs
 - Begrüßung
 - Vorstellungsrunde 
 - Website
 - Kommunikationswege
 - Erstis und Vorlesungen
 - Open Topic


> Marian Schnell eröffnet die Sitzung um 17:15 Uhr.

Vorstellungsrunde, damit sich alle Semester kennenlernen. 

Marian erläutert, was bisher geschehen ist:
Wir wollen unsere eigene Fachschaft sein um Rechte auf eigene Räume zu bekommen, sowie um den Studiengang als Gesamtes vertreten zu können. Im November 2020 wurden wir von der FRIV bestätigt und mussten nur noch vom Studierendenparlament bestätigt werden. Da wurde uns nicht mehr zurück gemeldet, wann das passiert ist. Darum wurden wir etwas überrascht und müssen jetzt unseren Pflichten nachkommen. Dazu zählt zum Beispiel die Website.

Thilo hat einen Aufschlag für eine Website gemacht, nachdem im letzten Jahr das schon Thema war.
Wir können bei der Rechnerbetriebsgruppe (RBG) kostenlos hosten.

Offene Frage: Sind wir beschlussfähig? Wir sind 18 Personen und wir haben die StuPa Satzung.


### Ziele der Website:
 - Terminkalender (unsere FSI, und der anderen Ma-Ph-In FS(I)s )
 - Ankündigung unserer Treffen
 - Veröffentlichung der Protokolle
 - Informationen für Studieninteressierte (Interviews mit aktuellen Studierenden, inhaltliche Schwerpunkte) *Wer Lust hat gerne melden!
 - Kontaktmöglichkeiten (FS(I)s wer wegen was) sowie zur Studienberatung und Scheuermann
 - Wie komme ich auf den FS Verteiler
 - Einstieg für Erstis (Erstellung von Accounts…)
 - Hinweise auf andere Angebote der Uni wie Hochschulsport, Sprachkurse
    
    

Marian macht eine Mail Liste beim CMS für uns als FSI oder wir übernehmen den Vorhandenen, damit mit uns Kontakt aufgenommen werden kann.


### Umsetzung: 
 - Hosting auf HU Servern (da kostenfrei) (RBG/CMS)
 - Domain ist ein zukünfiges Thema (wir werden erstmal eine vom CMS bekommen)
 - Bilder und Inhalte müssen zukünftig rechtlich von uns nutzbar sein
 - Seite muss leicht wartbar sein. Dokumentation!
 - Leichtgewichtig
 - Barrierefrei zugänglich
    
### Thilos Website:
 - Schon einiges vorhanden, was wir brauchen
 - Terminkalender crawlt z.B. die anderen FS(I)s Webseiten. 
    
Das Projekt soll auf [gitlab.informatik.hu-berlin.de](https://gitlab.informatik.hu-berlin.de/fsini-maphin/fachschaft-website) geteilt werden. Thilo gibt allen Interessierten Zugriff. Es gibt auch Stimmen, die sich für andere Technologien/Ansätze aussprechen, wie zum Beispiel MediaWiki.

### Hosting:
Wir wurden von der Informatik angesprochen, ob wir uns mit denen nicht eine VM teilen wollen um Arbeit zu teilen.
Die DEV Gruppe berichtet beim nächsten Termin über Updates. Das DEV Team trifft sich auf dem [IMP Discord](https://discord.gg/uX6nhTww).


## Nächstes Treffen
Wann soll das monatliche Treffen stattfinden?
Mittwochs - keine Gegenworte. Also jeder 3. Mittwoch im Monat. 
Uhrzeit: das nächste mal 18-20 Uhr. 

> Nächstes Treffen am 17.02.2021 von 18-20 Uhr


## Kommunikationskanäle
Als offizieller Kommunikationskanal sollte etwas gefunden werden, was datenschutzfreundlich vertretbar ist.
Es gibt von der Uni Matrix/Element, was von der HU selbst gehostet ist. Alle können sich dort mit dem HU Account einloggen.
Tim Lindstädt erstellt einen Matrix Raum.
> https://element.hu-berlin.de/#/room/#FS-MaPhIn:hu-berlin.de

> Die Fachschaft möge beschließen alle Kommunikationskanäle außer Matrix zu schließen.

| Dagegen | Enthaltung | Dafür | Ergebnis |
| :---------: | :-----------: | :-----: | :--------: |
|       0     |        2      |     13   | :white_check_mark: |

> Damit werden alle anderen Channels zukünftig geschlossen. Marian übernimmt das.

## Videokonferenzen
Es gibt von der Uni inzwischen eine [BigBlueButton (BBB) Instanz](https://bbb.hu-berlin.de/). Die Informatik hält dort die FS Sitzungen. Es hat einige Vorteile, wie zum Beispiel Meldungen in Reihenfolge.

> Die Fachschaft möge beschließen das nächste Treffen auf BigBlueButten zu machen.

| Dagegen | Enthaltung | Dafür | Ergebnis |
| :---------: | :-----------: | :-----: | :--------: |
|       0     |        4      |     8   | :white_check_mark: |

> Das nächste Treffen wird über BBB stattfinden. Link dazu folgt. 

## Satzung
was für eine Satzung haben wir. Ist das die Satzung des Studierendenparlaments https://vertretungen.hu-berlin.de/de/stupa/satzung §14 (6)

StuPa und FRIV werden angeschrieben, um in Erfahrung zu bringen, was wir noch alles zu tun haben. Jonas Bräuer kümmert sich darum.

## Sonstiges
Vermutlich kollidiert unserer Termin gerade mit der FRIV. Eventuell wird der also noch verschoben. Es wird dann über Matrix angekündigt.

Ende des offiziellen Teils und des Protokolls. In kleinen Gruppen wurde der Semesterstart für die Erstis, sowie die weitere Gestaltung der Website besprochen.

> Ende der Sitzung um 19:45 Uhr






