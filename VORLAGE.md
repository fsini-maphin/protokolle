# Protokoll des […] am dd.mm.yyyy

## Anwesende

\*: kommt später
(): ist zu Gast

## TOPs
 - Thema 1
 - Thema 2
 - …
 - Sonstiges

> Eröffnung des Treffens um hh:mm

### Thema 1



### Thema 2

>Die Fachschaft möge beschließen … .
>
| Dagegen | Enthaltung | Dafür | Ergebnis |
| :---------: | :-----------: | :-----: | :--------: |
|       a     |        b      |     c   | :white_check_mark: |

... ist dafür verantwortlich.

[//]: # (:white_check_mark:)
[//]: # (:x:)

### Sonstiges

> Ende des Treffen um hh:mm
